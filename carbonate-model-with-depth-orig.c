/* This code calculate the content and depth of pedogenic carbonate with depth.
   Later addition will be incorporating the spatial variable wetness index, dust fraction and residence time of the soil   */


#include<malloc.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>


#define NR_END 1
#define FREE_ARG char*

void nrerror(error_text)
char error_text[];
/* Numerical Recipes standard error handler */
{
        void exit();

        fprintf(stderr,"Numerical Recipes run-time error...\n");
        fprintf(stderr,"%s\n",error_text);
        fprintf(stderr,"...now exiting to system...\n");
        exit(1);
}

void free_ivector(int *v, long nl, long nh)
/* free an int vector allocated with ivector() */
{
        free((FREE_ARG) (v+nl-NR_END));
}

float *vector(nl,nh)
long nh,nl;
/* allocate a float vector with subscript range v[nl..nh] */
{
        float *v;

        v=(float *)malloc((unsigned int) ((nh-nl+1+NR_END)*sizeof(float)));
        if (!v) nrerror("allocation failure in vector()");
        return v-nl+NR_END;
}

int *ivector(nl,nh)
long nh,nl;
/* allocate an int vector with subscript range v[nl..nh] */
{
        int *v;

        v=(int *)malloc((unsigned int) ((nh-nl+1+NR_END)*sizeof(int)));
        if (!v) nrerror("allocation failure in ivector()");
        return v-nl+NR_END;
}



unsigned long *lvector(nl,nh)
long nh,nl;
/* allocate an unsigned long vector with subscript range v[nl..nh] */
{
	unsigned long *v;

	v=(unsigned long *)malloc((unsigned int) ((nh-nl+1+NR_END)*sizeof(long)));
	if (!v) nrerror("allocation failure in lvector()");
	return v-nl+NR_END;
}


#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

float ran3(idum)
int *idum;
{
        static int inext,inextp;
        static long ma[56];
        static int iff=0;
        long mj,mk;
        int i,ii,k;

        if (*idum < 0 || iff == 0) {
                iff=1;
                mj=MSEED-(*idum < 0 ? -*idum : *idum);
                mj %= MBIG;
                ma[55]=mj;
                mk=1;
                for (i=1;i<=54;i++) {
                        ii=(21*i) % 55;
                        ma[ii]=mk;
                        mk=mj-mk;
                        if (mk < MZ) mk += MBIG;
                        mj=ma[ii];
                }
                for (k=1;k<=4;k++)
                        for (i=1;i<=55;i++) {
                                ma[i] -= ma[1+(i+30) % 55];
                                if (ma[i] < MZ) ma[i] += MBIG;
                        }
                inext=0;
                inextp=31;
                *idum=1;
        }
        if (++inext == 56) inext=1;
        if (++inextp == 56) inextp=1;
        mj=ma[inext]-ma[inextp];
        if (mj < MZ) mj += MBIG;
        ma[inext]=mj;
        return mj*FAC;
}

#undef MBIG
#undef MSEED
#undef MZ
#undef FAC

// Functions to be written:

//Actual evapotrranspiration
//Potential evapotranspiration
//Temperature
//awc according to grain size

/*
// expdev

float expdev(long *idum)
{
	float ran1(long *idum);
	float dum;

	do
		dum=ran1(idum);
	while (dum == 0.0);
	return -log(dum);
}
*/


// Rainfall is a stochastic rainfall model that simulate the daily rainfall amount - rain [cm], and the interarrival time - inter [day] 
// based on the Gamma distribution of cumulative probabilities for rainfall amount and 
// interarrival time, by seasons, at the Desert Rock Site - Table A2 in Marion et al. (2008).
// In the future these pdf should be replaced with data from Daggett airport.

//function decleration
//float *RAIN(numberofyears);

//function header and body
//float *RAIN(int numberofyears);




// julian determines the julian date from the daily time stpe, begining in Jan. 1st. Input is day - number of days, output is the julian date

//function decleration
float JULIAN(day); // 

//function header and body
float JULIAN(long day)

{
	int julian;

	julian=day-((long)(day/365)*365);
	return julian;
}


// PCO2 is a function that calculates the partial pressure of CO2 in the soil atmosphere - pco2 [ppm] based on the soil depth [cm].
// We used equations derived from data presented in Table A1 of Marion et al. (2008).

//function decleration
float PCO2(d,julian,cthick); // 

//function header and body
float PCO2(int d,int julian,float cthick)

{
	float zt,zb,pco2,pco2t,pco2b;

	zt=(d-1)*cthick;
	zb=d*cthick;

	if (julian>68&julian<140) // spring
		{pco2t=(-0.037/3*pow(zt,3))+(15.36/2*pow(zt,2))+(653.36*zt); 
		 pco2b=(-0.037/3*pow(zb,3))+(15.36/2*pow(zb,2))+(653.36*zb); }
	else
		if (julian>139&julian<228) // summer
		{pco2t=(-0.053/3*pow(zt,3))+(16.39/2*pow(zt,2))+(698.60*zt); 
	     pco2b=(-0.053/3*pow(zb,3))+(16.39/2*pow(zb,2))+(698.60*zb); }
	else
		if (julian>227&julian<329) // fall
		{pco2t=(0.023/3*pow(zt,3))+(1.40/2*pow(zt,2))+(664.45*zt); 
	     pco2b=(0.023/3*pow(zb,3))+(1.40/2*pow(zb,2))+(664.45*zb); }
	else // winter
		{pco2t=(-0.042/3*pow(zt,3))+(9.33/2*pow(zt,2))+(775.62*zt); 
	     pco2b=(-0.042/3*pow(zb,3))+(9.33/2*pow(zb,2))+(775.62*zb); }
	
	pco2=(pco2b-pco2t)/cthick/pow(10,6); // divide by 10^6 to transfer from [ppm] to [atm]

return pco2;
}

// The functions before integration over the compartment thickness looks like:
	/*if (julian>68&julian<140) 
		pco2=(-0.037*pow(depth,2)+15.36*depth+653.36);
	else
		if (julian>139&julian<228) 
		pco2=(-0.053*pow(depth,2)+16.39*depth+698.60);
	else
		if (julian>227&julian<329) 
		pco2=(0.023*pow(depth,2)+1.40*depth+664.45);
		else
		pco2=(-0.042*pow(depth,2)+9.33*depth+775.62);*/



// solubility is a function that calculates the mass - caco3 [g] of CaCO3 and concentration of Ca in solution - cca [mol/L] or [M] in a compartment given the temprature - temp [deg]
// partial pressure of CO2 - pco2 [atm], intial concentration of Ca in solution - cca [mol/L] or [M], current soil moisture - moist [cm], previous soil moisture content - moisti [cm]
// initial mass of CaCO3 - caco3 [g], compartment thickness - thick [cm], and compartment area - area [cm2].
// The calculation is following Marion et al. (1985) and Hirmas et al. (2010)

//function decleration
float solubility(temp,pco2,cca,moist,moisti,caco3,cthick,area); // 

//function header and body
float solubility(float temp,float pco2,float cca,float moist,float moisti,float caco3,float cthick,float area)

{
	float k1,k2,k3,k4,I,g1,g2,A,B,C,ah0,ah1,ah,aco2,ahco3,aco3,aca,ccacalc,mcacalc,mcas0,mca0,mcaeq,mcas1,mca1;
	
	// Equilibrium constants for the carbonate-bicarbonate system as a function of temprature
	// Notation given by MArion et al. (1985)
	k1=pow(10,-1.14-(0.0131*temp)); // eq. 5
	k2=pow(10,-6.54+(0.0071*temp)); // eq. 7
	k3=pow(10,-10.59+(0.0102*temp)); // eq. 9
	k4=pow(10,-7.96-(0.0125*temp)); // eq. 11
	
	// Ionic strength
	I=3*cca; // eq. 14. as cca is in [M] or [mol/L], I is also in [M] or [mol/L] 

	// Activity coefficients for ionic valences of 1 and 2
	g1=pow(10,-0.505*(sqrt(I)/(1+sqrt(I))-(0.3*I))); // eq. 13
	g2=pow(10,-0.505*4*(sqrt(I)/(1+sqrt(I))-(0.3*I))); // eq. 13

	// Hydrogen ion activity
	A=2*k4/(g2*k3*k2*k1*pco2); // Left term in eq. 16
	B=k1*k2*pco2/g1; // Middle term in eq. 16
	C=2*k1*k2*k3*pco2/g2; // Right term in eq. 16

	// Newton's method for solving eq. 16
	ah0=pow(10,-8); // ph=8 intial guess
	ah1=pow(10,-4); // 
	while (abs(-1*log(ah1)+log(ah0))>0.1)
	 {
	  ah0=ah1;
	  ah1=ah0-((A*pow(ah0,4)-(B*ah0)-C)/((4*A*pow(ah0,3))-B));
	 }
	ah=ah1;

	// Equalibrium activities of CO2, HCO3, CO3, and Ca
	aco2=k1*pco2; // eq. 4
	ahco3=k2*aco2/ah; // eq. 6
	aco3=k3*ahco3/ah; // eq. 8
	aca=k4/aco3; // eq. 10

	// Ca concentration in [mol/L] or [M]
	ccacalc=aca/g2; // eq. 12
	
	// Determination of final grams of CaCO3 and Ca in solution within a compartment

	// Ca in solution in compartment volume [g]
	mcacalc=ccacalc*40.1*moist*area/1000; // molar mass of Ca is 40.1 [g/mol], moist is the water amount in [cm] of the compartment, area is in [cm^2], and dividing by 1000 is to transfer from Liter to cm^3

	// Ca in the soil phase in compartment volume [g/cm^3]
	mcas0=caco3/100.1*40.1*cthick*area; // molar mass of CaCO3 is 100.1 [g/mol]

	// Initial Ca in solution in compartment volume [g]
	// percolating from upper compartment + same compartment previous time step
	mca0=cca*40.1*moist/1000*area; // similar to calculating mcacalc

	// Ca needed in solution to reach equilibrium [g]		
	mcaeq=mcacalc-mca0;

	// Difference in Ca between Ca in soil phase to Ca in solution [g]
	mcas1=mcas0-mcaeq;

	if (mcas1>=0) mca1=mcacalc; // There is enough Ca in solution to reach equilibrium
	else						   // There is not enough Ca in solution to reach equilibrium	
	{mca1=mca0+mcas0; // Ca in solution equals Ca from solid + Ca from intial solution
	 mcas1=0;
	}
	
	
	// Conversions
	caco3=mcas1*100.1/40.1/cthick/area; // caco3 [g/cm3]
	cca=mca1/40.1/moist/area*1000; // Ca in solution [mol/L] or [M]

	return caco3;

}
//same as solubility function (above) but return cca instead of caco3
//function decleration
float solubilitycca(temp,pco2,cca,moist,moisti,caco3,cthick,area); // 

//function header and body
float solubilitycca(float temp,float pco2,float cca,float moist,float moisti,float caco3,float cthick,float area)

{
	float k1,k2,k3,k4,I,g1,g2,A,B,C,ah0,ah1,ah,aco2,ahco3,aco3,aca,ccacalc,mcacalc,mcas0,mca0,mcaeq,mcas1,mca1;
	
	// Equilibrium constants for the carbonate-bicarbonate system as a function of temprature
	// Notation given by MArion et al. (1985)
	k1=pow(10,-1.14-(0.0131*temp)); // eq. 5
	k2=pow(10,-6.54+(0.0071*temp)); // eq. 7
	k3=pow(10,-10.59+(0.0102*temp)); // eq. 9
	k4=pow(10,-7.96-(0.0125*temp)); // eq. 11
	
	// Ionic strength
	I=3*cca; // eq. 14

	// Activity coefficients for ionic valences of 1 and 2
	g1=pow(10,-0.505*(sqrt(I)/(1+sqrt(I))-0.3*I)); // eq. 13
	g2=pow(10,-0.505*4*(sqrt(I)/(1+sqrt(I))-0.3*I)); // eq. 13

	// Hydrogen ion activity
	A=2*k4/(g2*k3*k2*k1*pco2); // Left term in eq. 16
	B=k1*k2*pco2/g1; // Middle term in eq. 16
	C=2*k1*k2*k3*pco2/g2; // Right term in eq. 16

	// Newton's method for solving eq. 16
	ah0=pow(10,-8); // ph=8 intial guess
	ah1=pow(10,-4); // 
	while (abs(-1*log(ah1)+log(ah0))>0.1)
	 {
	  ah0=ah1;
	  ah1=ah0-((A*pow(ah0,4)-(B*ah0)-C)/((4*A*pow(ah0,3))-B));
	 }
	ah=ah1;

	// Equalibrium activities of CO2, HCO3, CO3, and Ca
	aco2=k1*pco2; // eq. 4
	ahco3=k2*aco2/ah; // eq. 6
	aco3=k3*ahco3/ah; // eq. 8
	aca=k4/aco3; // eq. 10

	// Ca concentration in [mol/L]
	ccacalc=aca/g2; // eq. 12
	
	// Determination of final grams of CaCO3 and Ca in solution within a compartment

	// Ca in solution in compartment volume [g]
	mcacalc=ccacalc*40.1*moist*area/1000;

	// Ca in the soil phase in compartment volume [g]
	mcas0=caco3/100.1*40.1*cthick*area;

	// Initial Ca in solution in compartment volume [g]
	// percolating from upper compartment + same compartment previous time step
	mca0=cca*40.1*moist/1000*area;

	// Ca needed in solution to reach equilibrium [g]		
	mcaeq=mcacalc-mca0;

	// Difference in Ca between Ca in soil phase to Ca in solution [g]
	mcas1=mcas0-mcaeq;

	if (mcas1>=0) mca1=mcacalc; // There is enough Ca in solution to reach equilibrium
	else						   // There is not enough Ca in solution to reach equilibrium	
	{mca1=mca0+mcas0; // Ca in solution equals Ca from solid + Ca from intial solution
	 mcas1=0;
	}
	
	
	// Conversions
	caco3=mcas1*100.1/40.1/cthick/area; // caco3 [g/cm3]
	cca=mca1/40.1/moist/area*1000; // Ca in solution [mol/L]

	return cca;

}


main()
{   FILE *fp2,*fp1,*fp3,*fp4,*fp5,*fp6,*fp7,*fp8;
	
	float temp,*pco2,*cca,*moist,*moisti,*caco3,area,ca,*thetawp,*whc,dust,caco3less,leachate,cca0,moisttotal,thetawptotal,whctotal,cthick;
	float k1,k2,k3,k4,I,g1,g2,A,B,C,ah0,ah1,ah,aco2,ahco3,aco3,aca,ccacalc,mcacalc,mcas0,mca0,mcaeq,mcas1;
	int i,d,depth,julian,w;
    
	int q,idum,numberofyears,interval,n,*sizeevent;
	float *rain,j,*cumprobrainwinter,*cumprobeventswinter,*sizerain;
	
	long day,duration,*rainevent,r;
	
	int month,year,*monthperday;
	float II,a,*T,*PET,*pan,*PETdaily,*pandaily,*AET,x,totalcaco3,initialmoisttotal,totalrain,totalAET,totalleachate,AETmemory,totalcarain,totalcasoil,totalcadust,totalcaleach;

	

	cca0=0.0000113; // Ca concentration in rainwater [M] 0.0000113 or [mol/L];
	area=1.0;
	depth=5;
	cthick=12.0;
	numberofyears=1000;
	dust=0.51/(365.0*10000.0); // carbonate in dust range from 0.5 to 5 [g m-2 yr-1]. I took 0.51 [g m-2 yr-1] = 5.1*10^-5 [g cm-2 yr-1] = 1.4*10^-7 [g cm-2 day-1]
	moisttotal=0.0;
	thetawptotal=0.0;
	whctotal=0.0;
	totalcaco3=0.0;
	totalrain=0.0;
	totalAET=0.0;
	totalleachate=0.0;
	totalcadust=0.0;
	totalcarain=0.0;
	totalcasoil=0.0;
	totalcaleach=0.0;


	// function RAINFALL - input is number of years and output is a vector of daily rainfall amount

     fp1=fopen("./cal/cumraindagg_winter.txt","r");  //input is probability of getting rainfall greater than x on any day, where x is in mm/d from 0.05 cm/d to 3.05 cm/d 
     fp2=fopen("./cal/rainfall_winter.txt","w"); // output the rinfall per day
	 fp3=fopen("./cal/averagerainfall_winter.txt","w"); // average annual rainfall 
	 fp4=fopen("./cal/cumraineventsdagg_winter.txt","r"); // input is probability of getting rainfall event greater than x, where x is in days from 1 day to 73 days 
	 fp5=fopen("./cal/rainfall&AETperday.txt","w");

	 
	 fp6=fopen("./cal/daggettmonthlytemp.txt","r");  //input is average monthly temprature in celcius
     fp7=fopen("./cal/PETandPEV.txt","w"); // output monthly and daily PET and PEV

	 fp8=fopen("./cal/caco3withdepth.txt","w");

	 idum=-10;                         //any neg integer
	 
	 
	 duration=numberofyears*365;                    //days of rainfall to produce
	 //duration=2000;

	 cumprobrainwinter=vector(1,61);
	 cumprobeventswinter=vector(1,73);
	
	 //carbonate=vector(1,duration);

	 monthperday=ivector(1,duration);
	 
	 moist=vector(1,depth);
	 moisti=vector(1,depth);
	 caco3=vector(1,depth);
	 thetawp=vector(1,depth);
	 whc=vector(1,depth);
	 pco2=vector(1,depth);
	 cca=vector(1,depth);

	 for (i=1;i<=61;i++)
	  fscanf(fp1,"%f",&cumprobrainwinter[i]);

	 for (i=1;i<=73;i++)
	  fscanf(fp4,"%f",&cumprobeventswinter[i]);

	 sizerain=vector(1,500000);
	 sizeevent=ivector(1,500000);
	 rain=vector(1,duration);
	 rainevent=lvector(1,500000);
	 
	 for (day=1;day<=duration;day++) rain[day]=0.0;

	 // loop that determines when a rain event will occur
	 n=1;
	 for (i=10000;i>=1;i--)
	  {if (cumprobeventswinter[n]>1.0*i/10000) n++; 
	  	   sizeevent[i]=n;} 
	 
	 // loop that determines how much rainfall will be in each rain day
	 j=0.05;
	 for (i=10000;i>=1;i--)
	  {if (cumprobrainwinter[(int)(j*20.0+1.0)]>1.0*i/5000) j=j+0.05; 
	  	   sizerain[i]=(j-0.05)+(0.05*ran3(&idum));} // for continuous values
	       //size[i]=j;} // for discrete values

	 interval=sizeevent[(int)(9999*ran3(&idum))+1];
	 day=1;
	 q=1;
	 fprintf(fp2,"day interval  rainfall  total\n");

     // loop for counting the days and detemine when will be a rain day and the amount of rainfall
	 while (day<=duration) 
	 {
		 if (day==1) 
			 {rain[day]=sizerain[(int)(9999*ran3(&idum))+1];
		      rainevent[q]=day;}
		 totalrain=totalrain+rain[day];
		 fprintf(fp2,"%d\t%d\t%0.3f\t%0.3f\n",day,interval,rain[day],totalrain);
		 day++;
		 if (day==rainevent[q]+interval)
		     {rain[day]=sizerain[(int)(9999*ran3(&idum))+1];
		      interval=sizeevent[(int)(9999*ran3(&idum))+1];
			  rainevent[q+1]=day;
			  q++;}
	 }
	 
	 fprintf(fp3,"average rainfall\n");
	 fprintf(fp3,"%0.3f\n",totalrain/numberofyears);
	



	 // initializing parameters with depth to all compartments

	 
	 for (d=1;d<=depth;d++) 
	 {thetawp[d]=0.039;        // weilding point (wp) is 3.9%
	  whc[d]=0.122-thetawp[d]; // field capacity (fc) is 12.2% and thus water holding capacity (whc) is set to 8.3% (difference between fc to wp)
	  //moisti[d]=(whc[d]/2+thetawp[d])*cthick; // initial moisture value set to mid value of whc, set to 8.05%, and incase of 10 thickness compartment, it is 0.805cm
	  moisti[d]=0.1*cthick;
	  moist[d]=moisti[d]; // moisture value set to the intial moisture value
	  caco3[d]=0.0;
	  pco2[d]=0.0;
	  cca[d]=cca0;
	  moisttotal=moisttotal+moisti[d]; // summing the total moisture in the soil profile. if there are 10 compartments of 10cm each, total moisture is 8.05cm 
	  whctotal=whctotal+(whc[d]*cthick); // summing the whc in the soil profile. if there are 10 compartments of 10cm each, total whc is 8.30cm 
	  thetawptotal=thetawptotal+(thetawp[d]*cthick); // summing the wp in the soil profile. if there are 10 compartments of 10cm each, total wp is 3.9cm 
	  initialmoisttotal=moisttotal;
	 }

	 
	 // The evapotranspiration model/function

	  
	 T=vector(1,12);
	 PET=vector(1,12);
	 pan=vector(1,12);
	 PETdaily=vector(1,12);
	 pandaily=vector(1,12);
	 
	 AET=vector(1,duration);
		  
	 II=0.0;
	 ca=0.0;

	 for (month=1;month<=12;month++)	// reads from a file the avg. monthly temp. in Daggett, CA and calculates I, the annual heat index
		 {fscanf(fp6,"%f",&T[month]);
	      II=II+pow((T[month]/5.0),1.514);}  

	 a=(6.75*pow(10.0,-7.0)*pow(II,3.0))-(7.71*pow(10.0,-5.0)*pow(II,2.0))+(0.01792*II)+0.49239;
	 fprintf(fp7,"month	m.PET  m.pan ET d.PET d.pan ET\n");
	
	 for (month=1;month<=12;month++) // calculates PET [cm/month] according to Thornthwaite (1948) and PEV [cm/month] according to equations in figure 3 of Marion et al (1985)
		 {PET[month]=1.6*pow((10.0*T[month]/II),a);
	      PETdaily[month]=PET[month]/30.0;
	      if (month<=7) 
			  {pan[month]=((17.07*exp(-0.1309*T[month]))+1.91)*PET[month];
		       pandaily[month]=((17.07*exp(-0.1309*T[month]))+1.91)*PETdaily[month];}
		  else
			  {pan[month]=((13.67*exp(-0.13*T[month]))+1.35)*PET[month];
		      pandaily[month]=((13.67*exp(-0.13*T[month]))+1.35)*PETdaily[month];}
		  //pandaily[month]=pan[month]/30.0;
		  fprintf(fp7,"%d\t%0.3f\t%0.3f\t%0.3f\t%0.3f\n",month,PET[month],pan[month],PETdaily[month],pandaily[month]);}
	 
	
	
	 r=0;
	for (day=1;day<=duration;day++)
	{			
        r=JULIAN(day);
		//r=(day-((int)(day/365.01)*365));
		if (r<=31)
			monthperday[day]=1;
		else
			if (r<=59)
            monthperday[day]=2;
			else
				if (r<=90)
                monthperday[day]=3;
				else
                    if (r<=120)
                    monthperday[day]=4;
		            else
                        if (r<=151)
                        monthperday[day]=5;
						else
							if (r<=181)
							monthperday[day]=6;
							else
								if (r<=212)
								monthperday[day]=7;
								else
									if (r<=243)
									monthperday[day]=8;
									else
										if (r<=273)
										monthperday[day]=9;
										else
											if (r<=304)
											monthperday[day]=10;
											else
												if (r<=334)
												monthperday[day]=11;
												else
													if (r<=365)
													monthperday[day]=12;

	}
	/*
	if (moisttotal>=((0.55*whctotal)+thetawptotal)) // according to Marion et al. (1985), for the upper 45% of the total whc the actual evapotranspiration (AET) is the potential evapotranspiration (pet)
		AET[day]=PETdaily[monthperday[day]];
		else
		AET[day]=(moisttotal-thetawptotal)/whctotal*pandaily[monthperday[day]];
	fprintf(fp5,"%ld\t%ld\t%0.3f\t%0.3f\n",day,r,rain[day],AET[day]);
	*/
	

	

     // This is the main program....
	
	 
	 fprintf(fp5,"day   rainfall    panE	moist	AET\n");

    for (day=1;day<=duration;day++) // the main loof that counts the days
    {
	temp=T[monthperday[day]];      // set the temprature for the day according to the monthly average
	julian=JULIAN(day);
	pco2[1]=PCO2(1,julian,cthick);  // set the pco2 value fo the 1st compartment according to the date
	
	totalcadust=totalcadust+dust;

	totalcarain=totalcarain+(rain[day]*cca0*40.0/1000.0);

	if (moisttotal>=((0.546*whctotal)+thetawptotal)) // according to Marion et al. (1985), for the upper 45% of the total whc the actual evapotranspiration (AET) is the potential evapotranspiration (pet)
		AET[day]=pandaily[monthperday[day]];		// in case of 10 compartments of 10 cm each, if total moistute > 8.465 
		//AET[day]=PETdaily[monthperday[day]];
	    else										// the lower 55% of the total whc are according to modifeid Thornthwaite-Mather model
		AET[day]=(moisttotal-thetawptotal)/(0.546*whctotal)*pandaily[monthperday[day]];
		//AET[day]=((moisttotal*0.352)-1.36)*pandaily[monthperday[day]];
	
	AETmemory=AET[day];
	totalAET=totalAET+AET[day];

	//fprintf(fp5,"%ld\t%0.3f\t%0.3f\t%0.3f\t%0.3f\n",day,rain[day],pandaily[monthperday[day]],moisttotal,AET[day]);

	
	//totalrain=totalrain+rain[day];
	
	

	moist[1]=moisti[1]+rain[day];   // set the moiture of the 1st compartment to the intial moisture plus the daily rainfall. rainfall is added only the 1st compartment
	
	if ((moist[1]-(thetawp[1]*cthick))<AET[day]) // examine if the daily AET is greater than the available water for evaporation at the 1st compartment
	    {AET[day]=AET[day]-(moist[1]-(thetawp[1]*cthick));           // if yes, evaporate all the available moisture and set the moisture to the wilting point
	     moist[1]=thetawp[1]*cthick;}
	else
	    {moist[1]=moist[1]-AET[day];             // if no, substract the AET from the moisture of the 1st compartment
	     AET[day]=0.0;}
	    // in both cases, the AET is updated: if the AET is greater than the available water for evaporation at the 1st compartment, than
	    // substract the part that was evaporated from the AET. Other wise, set AET to zero. 
	 
	
	//fprintf(fp5,"%0.3f\t%0.3f\n",moist[1],AET[day]);


	caco3[1]=caco3[1]+(dust/cthick); // add the daily dust accumulation to the 1st compartment
	

	if (((thetawp[1]+whc[1])*cthick)<moist[1]) // examine if the updated moisture of the 1st compartment (after rainfall addition and AET substraction)
											   // is greater than field capacity (=saturation). if yes, starts the solubility calculation
	{
	 ca=solubility(temp,pco2[1],cca[1],moist[1],moisti[1],caco3[1],cthick,area); // calculates the carbonate solubility for the 1st compartment. ca equals to the amount of caco3 [g/cm3]
	 if (caco3[1]>ca) caco3less=caco3[1]-ca; // if the caco3 in the 1st compartment is greater than the soluble caco3 (=ca) then caco3less is set to the difference between them
	 else caco3less=0.0;                         // if not, caco3less is set to zero. note that caco3less is the dissolution/percipitation amount of carbonate
	 caco3[1]=caco3[1]-caco3less;
	 caco3[2]=caco3[2]+caco3less;	
	}
	
	
	// This is the second loop that runs through the soil profile, from the 2nd compartment to the bottom
	for (d=2;d<=depth;d++)
		{
		 pco2[d]=PCO2(d,julian,cthick);   // set the pco2 values to all compartments
		
		 if (moist[d-1]>((whc[d-1]+thetawp[d-1])*cthick)) leachate=moist[d-1]-((whc[d-1]+thetawp[d-1])*cthick); // determines the leachate from the upper compartment by substracting the field capacity from the moisture content
		 else leachate=0.0; // if there is no excess water, then the leachate is zero
	     moist[d-1]=moist[d-1]-leachate; // subtract the leachate from the moisture of the upper compartment
		 moist[d]=moisti[d]+leachate;    // add the leachate to the moisture of the current compartment
		
		 if (moist[d]-(thetawp[d]*cthick)<AET[day]) // same as for the 1st compartment, taking into account the AET for this current compartment, and updating the AET value
		    {AET[day]=AET[day]-(moist[d]-(thetawp[d]*cthick));
		     moist[d]=thetawp[d]*cthick;}
		 else
		    {moist[d]=moist[d]-AET[day];
		     AET[day]=0.0;}
		    
		 if (((thetawp[d]+whc[d])*cthick)<moist[d]) // same as for the 1st compartment, examine if we reached saturation
		    {
			 ca=solubility(temp,pco2[d],cca[d],moist[d],moisti[d],caco3[d],cthick,area);
			 if (caco3[d]>ca) caco3less=caco3[d]-ca;
	         else caco3less=0.0;
	         caco3[d]=caco3[d]-caco3less;
	         if (d!=depth) caco3[d+1]=caco3[d+1]+caco3less;
			 else totalcaleach=totalcaleach+caco3less;
		    }
		 
			if (d==depth) // examines if we reached the compartment above the lowest one 
			   {if (AET[day]>0) totalAET=totalAET-AET[day];
			   	
			 //   if (moist[d+1]>((whc[d+1]+thetawp[d+1])*cthick)) 
			 //   {moist[d+1]=moist[d+1]-(moist[d+1]-(whc[d+1]+thetawp[d+1])*cthick); // calculate the moisture for the last compartment. if it is above field capacity, substracts the excess water from the moisture of the lowest compartment (i.e. the water is leached to the rock)
			 //    totalleachate=totalleachate+(moist[d+1]-(whc[d+1]+thetawp[d+1])*cthick);}
			 //   else
				//{moist[d+1]=moist[d+1]-0.0;}

				 if (moist[d]>((whc[d]+thetawp[d])*cthick)) 
			        leachate=moist[d]-((whc[d]+thetawp[d])*cthick); // determines the leachate from the upper compartment by substracting the field capacity from the moisture content
		             else leachate=0.0; // if there is no excess water, then the leachate is zero
	             moist[d]=moist[d]-leachate; // subtract the leachate from the moisture of the upper compartment
		 	     totalleachate=totalleachate+leachate;
			    

			    moisttotal=0.0;
			    //pco2[d+1]=PCO2(d+1,julian,cthick);
			    for (w=1;w<=depth;w++) // resert the initial soil moisture as the current soil moisture, and calculate the total moisture
			    {moisti[w]=moist[w];
			     moisttotal=moisttotal+moisti[w];
			     //fprintf(fp5,"%d\t%0.3f\n",w,moist[w]);
			     }
			   
			 //fprintf(fp5,"%ld\t%0.3f\t%0.3f\t%0.3f\t%0.3f\n",day,rain[day],pandaily[monthperday[day]],moisttotal,AET[day]);

			    }
		  
	     }
		
    }
	
	fprintf(fp8,"time (years) = ");
	fprintf(fp8,"%d\n\n",numberofyears);
	fprintf(fp8,"depth(cm)       pco2(atm)              caco3(g/cm3)		caco3(g/cm2)		caco3(g/m2)\n");
	for (d=1;d<=depth;d++)
	{fprintf(fp8,"%0.0f-%0.0f\t\t%0.6f\t\t%0.6f\t\t\%0.6f\t\t\%0.1f\n",(d-1)*cthick,d*cthick,pco2[d],caco3[d],caco3[d]*cthick,caco3[d]*cthick*10000);
	totalcaco3=totalcaco3+caco3[d]*cthick;}
	//fprintf(fp8,"\n%0.3f\n",totalcaco3);
	fprintf(fp8,"\nCa mass balance (g/cm2):\n");
	fprintf(fp8,"Ca accumulation from dust	Ca accumulation from rain	Ca in soil as CaCO3	leachate Ca\n");
	fprintf(fp8,"\t%0.5f\t\t\t\t%0.5f\t\t\t\t%0.5f\t\t%0.5f\n",0.4*totalcadust,totalcarain,totalcaco3*0.4,totalcaleach*0.4);
	fprintf(fp8,"balance = ");
	fprintf(fp8,"%0.5f\n\n",(0.4*totalcadust)+totalcarain-(totalcaco3*0.4)-(totalcaleach*0.4));
	fprintf(fp8,"water mass balance (cm):\n");
	fprintf(fp8,"intial soil water	rainfall	final soil water	AET		leachate\n");
	fprintf(fp8,"\t%0.2f\t\t%0.2f\t\t%0.2f\t\t\t%0.2f\t\t%0.2f\n",initialmoisttotal,totalrain,moisttotal,totalAET,totalleachate);
	fprintf(fp8,"balance = ");
	fprintf(fp8,"%0.3f\n",initialmoisttotal+totalrain-moisttotal-totalAET-totalleachate);
	
	
	//totalcarain=totalcarain+(rain[day]*cca0*40.0/1000.0);

fclose(fp1);
fclose(fp2);
fclose(fp3);
fclose(fp4);
fclose(fp5);
fclose(fp6);
fclose(fp7);
fclose(fp8);

}

