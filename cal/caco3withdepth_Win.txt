time (years) = 1000

depth(cm)       pco2(atm)              caco3(g/cm3)		caco3(g/cm2)		caco3(g/m2)
0-12		0.000830		0.000001		0.000009		0.1
12-24		0.000929		0.000050		0.000606		6.1
24-36		0.001017		0.003094		0.037130		371.3
36-48		0.001093		0.001105		0.013256		132.6
48-60		0.001156		0.000000		0.000000		0.0

Ca mass balance (g/cm2):
Ca accumulation from dust	Ca accumulation from rain	Ca in soil as CaCO3	leachate Ca
	0.02050				0.00362				0.02040		0.00000
balance = 0.00372

water mass balance (cm):
intial soil water	rainfall	final soil water	AET		leachate
	6.00		8009.98		2.44			8012.09		0.00
balance = 1.456
